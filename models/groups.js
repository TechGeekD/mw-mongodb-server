var mongoose= require('mongoose');

const GroupSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    member: {
        type: mongoose.Schema.ObjectId,
        ref: 'users'
    },
    type: {
        type: String,
        required: true
    }
});

const groups = module.exports = mongoose.model('Groups', GroupSchema);