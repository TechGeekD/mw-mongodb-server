const mongoose = require('mongoose');

const EventSchema = mongoose.Schema({
    name: { 
        type: String,
        required: true,
    },
    desc: { 
        type: String,
        required: true,
    },
    category: { 
        type: String,
        required: true,
    },
    city: { 
        type: String,
        required: true,
    },
    date: { 
        type: String,
        required: true,
    },
    manager: { 
        type: String,
        required: false,
    },
    interest: { 
        type: String,
        required: false,
    },
    cover: { 
        type: String,
        required: false,
    },
    repo_points: { 
        type: String,
        required: true,
    },
});

const events = module.exports = mongoose.model('Events', EventSchema);