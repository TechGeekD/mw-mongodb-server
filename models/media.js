var mongoose = require('mongoose');

const MediaSchema = mongoose.Schema({
    url: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    ev_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'events',
        required: true
    }
});

const media = module.exports = mongoose.model('Media', MediaSchema);