var mongoose = require('mongoose');

const UserEventsSchema = mongoose.Schema({
    user_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'users',
        default: null
    },
    group_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'groups',
        default: null
    },
    ev_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'events'
    },
    leader: {
        type: mongoose.Schema.ObjectId,
        ref: 'users'
    },
    type: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    }
});

const usersEvents = module.exports = mongoose.model('Users_Events', UserEventsSchema);