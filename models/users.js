const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    username: { 
        type: String,
        required: true,
    },
    fullname: { 
        type: String,
        required: true,
    },
    email: { 
        type: String,
        required: true,
    },
    password: { 
        type: String,
        required: true,
    },
    phone: { 
        type: String,
        required: false,
    },
    status: { 
        type: String,
        required: true,
    },
    dp: { 
        type: String,
        required: false,
    },
    nid: { 
        type: String,
        required: false,
    },
    repo_points: { 
        type: String,
        required: false,
    },
    rfid: { 
        type: String,
        required: false,
    },
});

const users = module.exports = mongoose.model('Users', UserSchema);