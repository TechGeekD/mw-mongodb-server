var mongoose = require('mongoose');

const RequestSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    desc: {
        type: String,
        required: true
    },
    user_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'users',
        required: true
    },
    category: {
        type: String,
        required: true
    },
    media: {
        type: String,
        required: false
    },
    type: {
        type: String,
        required: false
    },
    status: {
        type: String,
        required: false
    }
});

const request = module.exports = mongoose.model('Requests', RequestSchema);