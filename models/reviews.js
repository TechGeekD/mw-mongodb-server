const mongoose = require('mongoose');

const ReviewSchema = mongoose.Schema({
    review: {
        type: String,
        required: true
    },
    user_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'users',
        required: true
    },
    ev_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'events',
        required: true
    }
});

const review = module.exports = mongoose.model('Review', ReviewSchema);