var mongoose = require('mongoose');

const AttendanceSchema = mongoose.Schema({
    user_id: { 
        type: mongoose.Schema.ObjectId, 
        ref: 'users' 
    },
    date: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        required: true,
    },
    ev_id: { 
        type: mongoose.Schema.ObjectId, 
        ref: 'events' 
    },
    rfid: {
        type: String,
        required: true,
    }
});

const attendance = module.exports = mongoose.model("Attendance", AttendanceSchema);