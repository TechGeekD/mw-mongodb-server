var mongoose = require('mongoose');

const UserInterestSchema = mongoose.Schema({
    user_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'users',
        required: true
    },
    interest: {
        type: String,
        required: true
    }
});

const userInterests = module.exports = mongoose.model('User_Interests', UserInterestSchema);