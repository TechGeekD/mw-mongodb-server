var express = require('express');
var mongoose = require('mongoose');
var bodyparser = require('body-parser');
var cors = require('cors');
var path = require('path');

var app = express();

//Dynamic PORT Number or SET DEFAULT 3030
const port = process.env.PORT || 3030;
const routes = require('./routes/routes');
const auth = 'mongodb://techgeekd:48122112@';
const server = 'ds113713.mlab.com:13713/metta';
app.use(cors());

app.use(bodyparser.json());

app.use(express.static(path.join(__dirname, 'public')));

// mongoose.connect('mongodb://localhost:27017/metta', {
//     useMongoClient: true,
// });

mongoose.connect(auth + server, {
    useMongoClient: true,
});

mongoose.connection.on('connected', () => {
    console.log('MongoDB Connected To:', server);
});

mongoose.connection.on('error', (err) => {
    if(err){
        console.log('ERROR IN CONNECTION: ', err);
    }
});

app.use('/api', routes);

app.get( '/', (req, res) => {
    res.send('foobar');
});

app.listen(port, () => {
    console.log('Started Listening on Port: ', port);
});