const express =  require('express');
const router =  express.Router();
var bcrypt = require('bcrypt');
var mongoose = require('mongoose');

const Users = require('../models/users');
const Media = require('../models/media');
const Events = require('../models/events');
const Groups = require ('../models/groups');
const Reviews = require('../models/reviews');
const Requests = require('../models/request');
const Attendance = require('../models/attendance');
const UsersEvents = require('../models/users_events');
const UserInterests = require('../models/user_interests');

const ObjectId = mongoose.Types.ObjectId;
const saltRounds = 10;

//-GET METHODS

    //GET ALL EVENTS
    router.get('/events', (req, res, next) => {
        Events.find((err, events)=>{
            res.json(events);
        });
    });

    //GET SPECIFIC EVENT BY ID
    router.get('/event/:id', (req, res, next) => {
        Events.findById(req.params.id, (err, event)=>{
            res.json(event);
        });
    });

    //GET ALL USERS
    router.get('/users', (req, res, next) => {
        Users.find((err, users) => {
            res.json(users);
        });
    });

    //GET SPECIFIC USER BY ID
    router.get('/user/:id', (req, res, next) => {
        Users.findById(req.params.id, (err, user)=>{
            res.json(user);
        });
    });

    //GET ALL ATTENDANCE DETAILS
    router.get('/attend', (req, res, next) => {
        Attendance.aggregate({
            $lookup: {
                from: "users",
                localField: "user_id",
                foreignField: "_id",
                as: "user_id"
            }
        }, { $unwind: "$user_id" },
        {
            $lookup: {
                from: "events",
                localField: "ev_id",
                foreignField: "_id",
                as: "ev_id"
            }
        }, { $unwind: "$ev_id" }, (err, result) => {
            res.json(result);
        });
    });

    //GET SPECIFIC ATTENDANCE DETAILS
    router.get('/attend/:id', (req, res, next) => {

        Attendance.aggregate({
             $match: { '_id': ObjectId(req.params.id) }
        },{
            $lookup: {
                from: "users",
                localField: "user_id",
                foreignField: "_id",
                as: "user_id"
            }
        },{ $unwind: "$user_id" },
        {
            $lookup: {
                from: "events",
                localField: "ev_id",
                foreignField: "_id",
                as: "ev_id"
            }
        },{ $unwind: "$ev_id" }, (err, result) => {
            console.log(result[0].ev_id.name);
            res.json(result);
        });
    });

    //GET USERS ALL EVENTS
    router.get('/usersEvents', (req, res, next) => {
        UsersEvents.aggregate({ 
            $lookup: {
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                as: 'user_id'
            }
        }, { $unwind: { path: '$user_id', "preserveNullAndEmptyArrays": true } },
        {
            $lookup: {
                from: 'groups',
                localField: 'group_id',
                foreignField: '_id',
                as: 'group_id'
            }
        }, { $unwind: { path: '$group_id', "preserveNullAndEmptyArrays": true } }, 
        {        
            $lookup: {
                from: 'events',
                localField: 'ev_id',
                foreignField: '_id',
                as: 'ev_id'
            }
        }, { $unwind: '$ev_id' },
        {
            $lookup: {
                from: 'users',
                localField: 'leader',
                foreignField: '_id',
                as: 'leader'
            }
        }, { $unwind: '$leader' }, (err, result) => {
            res.json(result);
        });
    });

    //GET USERS SPECIFIC EVENT
    router.get('/usersEvents/:id', (req, res, next) => {
        UsersEvents.aggregate({
            $match: { '_id': ObjectId(req.params.id) }
        },{ 
            $lookup: {
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                as: 'user_id'
            }
        }, { $unwind: { path: '$user_id', "preserveNullAndEmptyArrays": true } },
        {
            $lookup: {
                from: 'groups',
                localField: 'group_id',
                foreignField: '_id',
                as: 'group_id'
            }
        }, { $unwind: { path: '$group_id', "preserveNullAndEmptyArrays": true } }, 
        {        
            $lookup: {
                from: 'events',
                localField: 'ev_id',
                foreignField: '_id',
                as: 'ev_id'
            }
        }, { $unwind: '$ev_id' },
        {
            $lookup: {
                from: 'users',
                localField: 'leader',
                foreignField: '_id',
                as: 'leader'
            }
        }, { $unwind: '$leader' }, (err, result) => {
            res.json(result);
        });
    });

    //GET ALL GROUPS
    router.get('/groups', (req, res, next) => {
        Groups.aggregate({
            $lookup: {
                from: 'users',
                localField: 'member',
                foreignField: '_id',
                as: 'member'

            }
        }, (err, result) => {
            res.json(result);
        });
    });

    //GET SPECIFIC GROUP
    router.get('/groups/:id', (req, res, next) => {
        Groups.aggregate({
            $match: { '_id': ObjectId(req.params.id) } 
        },
        {
            $lookup: {
                from: 'users',
                localField: 'member',
                foreignField: '_id',
                as: 'member'

            }
        }, (err, result) => {
            res.json(result);
        });
    });

    //GET ALL REQUESTS
    router.get('/requests', (req, res, next) => {
        Requests.aggregate({
            $lookup: {
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                as: 'user_id'
            }
        }, (err, result) => {
            res.json(result);
        });
    });

    //GET SPECIFIC REQUESTS
    router.get('/requests/:id', (req, res, next) => {
        Requests.aggregate({
            $match: { '_id': ObjectId(req.params.id) }
        },
        {
            $lookup: {
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                as: 'user_id'
            }
        }, (err, result) => {
            res.json(result);
        });
    });

    //GET ALL MEDIA
    router.get('/medias', (req, res, next) => {
        Media.aggregate({
            $lookup: {
                from: 'events',
                localField: 'ev_id',
                foreignField: '_id',
                as: 'ev_id'
            }
        }, (err, result) => {
            res.json(result);
        });
    });

    //GET SPECIFIC MEDIA
    router.get('/medias/:id', (req, res, next) => {
        Media.aggregate({
            $match: { '_id': ObjectId(req.params.id) }
        },
        {
            $lookup: {
                from: 'events',
                localField: 'ev_id',
                foreignField: '_id',
                as: 'ev_id'
            }
        }, (err, result) => {
            res.json(result);
        });
    });

    //GET ALL USER INTERESTS
    router.get('/userInterests', (req, res, next) => {
        UserInterests.aggregate({
            $lookup: {
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                as: 'user_id'
            }
        }, (err, result) => {
            res.json(result);
        });
    });

    //GET SPECIFIC USER INTERESTS
    router.get('/userInterests/:id', (req, res, next) => {
        UserInterests.aggregate({
            $match: { '_id': ObjectId(req.params.id) }
        },
        {
            $lookup: {
                from: 'users',
                localField: 'user_id',
                foreignField: '_id',
                as: 'user_id'
            }
        }, (err, result) => {
            res.json(result);
        });
    });

    //GET ALL REVIEWS DETAILS
    router.get('/reviews', (req, res, next) => {
        Reviews.aggregate({
            $lookup: {
                from: "users",
                localField: "user_id",
                foreignField: "_id",
                as: "user_id"
            }
        }, { $unwind: "$user_id" },
        {
            $lookup: {
                from: "events",
                localField: "ev_id",
                foreignField: "_id",
                as: "ev_id"
            }
        }, { $unwind: "$ev_id" }, (err, result) => {
            res.json(result);
        });
    });

    //GET SPECIFIC REVIEWS DETAILS
    router.get('/reviews/:id', (req, res, next) => {
        Reviews.aggregate({
            $match: { '_id': ObjectId(req.params.id) }
        },
        {
            $lookup: {
                from: "users",
                localField: "user_id",
                foreignField: "_id",
                as: "user_id"
            }
        }, { $unwind: "$user_id" },
        {
            $lookup: {
                from: "events",
                localField: "ev_id",
                foreignField: "_id",
                as: "ev_id"
            }
        }, { $unwind: "$ev_id" }, (err, result) => {
            res.json(result);
        });
    });

//-POST METHODS

    //ADD NEW EVENT
    router.post('/event', (req, res, next) => {
        const newEvent = new Events({
            name: req.body.name,
            desc: req.body.desc,
            category: req.body.category,
            city: req.body.city,
            date: req.body.date,
            manager: req.body.manager,
            interest: req.body.interest,
            cover: req.body.cover,
            repo_points: req.body.repo_points
        });
        newEvent.save( (err, result) => {
            if(err) {
                res.json({msg: "Failed to Add Contact"});
            } else {
                res.json({msg: "Contact Added Succsessfully"});
            }
        });
    });

    //ADD NEW USER
    router.post('/user', (req, res, next) => {
        var password = '';
        bcrypt.genSalt(saltRounds, function(err, salt) {
            bcrypt.hash(req.body.password, salt, function(err, hash) {
                password = hash;
                
                const newUser = new Users({
                    username: req.body.username,
                    fullname: req.body.fullname,
                    email: req.body.email,
                    password: password,
                    phone: req.body.phone,
                    status: req.body.status,
                    dp: req.body.dp,
                    nid: req.body.nid,
                    repo_points: req.body.repo_points,
                    rfid: req.body.rfid
                });

                newUser.save( (err, result) => {
                    if (err) {
                        res.json({msg: "Failed To Add User", error: err});
                    } else {
                        res.json({msg: "User Added Succsessfully"});
                    }
                });
            });
        });

    });

    //ADD USER ATTENDANCE
    router.post('/attend', (req, res, next) => {
        const newAttend = new Attendance({
            user_id: req.body.user_id,
            date: req.body.date,
            status: req.body.status,
            ev_id: req.body.ev_id,
            rfid: req.body.rfid
        });
        newAttend.save((err, result) => {
            if (err) {
                res.json({msg: 'Attendance Failed', error: err});
            } else {
                res.json({msg: 'Attendance Successfull'});
            }
        });
    });

    //ADD USER'S EVENT
    router.post('/userEvent', (req, res, next) => {
        const newUserEvent = new UsersEvents({
            user_id: req.body.user_id,
            group_id: req.body.group_id,
            ev_id: req.body.ev_id,
            leader: req.body.leader,
            type: req.body.type,
            status: req.body.status
        });

        newUserEvent.save((err, result) => {
            if (err) {
                res.json({msg: "User Event Add Failed", error: err});
            } else {
                res.json({msg: "User Event Add Successfull"});
            }
        });
    });

    //CREATE GROUP
    router.post('/group', (req, res, next) => {
        const newGroup = new Groups({
            name: req.body.name,
            member: req.body.member,
            type: req.body.type
        });

        newGroup.save((err, result) => {
            if (err) {
                res.json({msg: 'Group Creation Failed', error: err});
            } else {
                res.json({msg: 'Group Created Successfully'});
            }
        });
    });

    //ADD REQUEST
    router.post('/request', (req, res, next) => {
        const newRequest = new Requests({
            title: req.body.title,
            desc: req.body.desc,
            user_id: req.body.user_id,
            category: req.body.category,
            media: req.body.media,
            type: req.body.type,
            status: req.body.status
        });

        newRequest.save((err, result) => {
            if (err) {
                res.json({msg: 'Request Submit Failed'});
            } else { 
                res.json({msg: 'Request Submit Successfull'});
            }
        });
    });

    //ADD MEDIA
    router.post('/media', (req, res, next) => {
        const newMedia = new Media({
            url: req.body.url,
            type: req.body.type,
            category: req.body.category,
            ev_id: req.body.ev_id
        });
        newMedia.save((err, result) => {
            if (err) {
                res.json({msg: "Media Upload Failed"});
            } else {
                res.json({msg: "Media Upload Successfull"})
            }
        });
    });

    //ADD USER INTEREST
    router.post('/userInterest', (req, res, next) => {
        const newUserInterest = new UserInterests({
            user_id: req.body.user_id,
            interest: req.body.interest,
        });
        newUserInterest.save((err, result) => {
            if (err) {
                res.json({msg: "Failed To Add UserInterest"});
            } else {
                res.json({msg: "UserInterest Add Successfull"})
            }
        });
    });

    //ADD REVIEW
    router.post('/review', (req, res, next) => {
        const newReview = new Reviews({
            review: req.body.review,
            user_id: req.body.user_id,
            ev_id: req.body.ev_id,
        });
        newReview.save((err, result) => {
            if (err) {
                res.json({msg: "Failed To Add Review"});
            } else {
                res.json({msg: "Review Add Successfull"})
            }
        });
    });

    //LOGIN USER
    router.post('/login', (req, res, next) => {

        console.log(req.body);
        if (req.body.username) {
            Users.findOne({ 'username': req.body.username }, (err, result) => {
                if (err || !result ) {
                    res.json({msg: 'LOGIN ERROR', error: err});
                } else {
                    bcrypt.compare(req.body.password, result.password, function(err, success) {
                        if ( success ) {
                            res.json({msg: 'Success Login', res: result});                    
                        } else if (err) {
                            res.json({msg: 'Login Check Error', error: err});                    
                        } else {
                            res.json({msg: 'Login Failed'});                    
                        }
                    });
                }
            });
        } else {
            res.json({msg: 'LOGIN ERROR', error: 'Provide Login Data'});
        }
    });

//-PUT METHODS

    //UPDATE EVENTS DETAILS
    router.put('/event/:id', (req, res, next) => {
        Events.findByIdAndUpdate(req.params.id, req.body, (err, result) => {
            res.json(result);
        });
    });

    //UPDATE USERS DETAILS
    router.put('/user/:id', (req, res, next) => {

        if ( req.body.password ) {
            bcrypt.genSalt(saltRounds, function(err, salt) {
                bcrypt.hash(req.body.password, salt, function(err, hash) {
                    req.body.password = hash
                    Users.findByIdAndUpdate(req.params.id, req.body, (err, result) => {
                        res.json(result);
                    });
                });
            });
        } else {
            Users.findByIdAndUpdate(req.params.id, req.body, (err, result) => {
                res.json(result);
            });
        }
    });

    //UPDATE USER EVENT DETAILS
    router.put('/userEvent/:id', (req, res, next) => {
        UsersEvents.findByIdAndUpdate(req.params.id, req.body, (err, result) => {
            res.json(result);
        });
    });

    //UPDATE GROUPS DETAILS
    router.put('/gorup/:id', (req, res, next) => {
        Groups.findByIdAndUpdate(req.params.id, req.body, (err, result) => {
            res.json(result);
        });
    });

    //UPDATE REQUEST 
    router.put('/request/:id', (req, res, next) => {
        Requests.findByIdAndUpdate(req.params.id, req.body, (err, result) =>{
            res.json(result);
        });
    });

    //UPDATE USER INTEREST 
    router.put('/userInterest/:id', (req, res, next) => {
        UserInterests.findByIdAndUpdate(req.params.id, req.body, (err, result) =>{
            res.json(result);
        });
    });

    //UPDATE REVIEW
    router.put('/review/:id', (req, res, next) => {
        Reviews.findByIdAndUpdate(req.params.id, req.body, (err, result) =>{
            res.json(result);
        });
    });

//-DELETE METHODS

    //REMOVE EVENT
    router.delete('/event/:id', (req, res, next) => {
        Events.findByIdAndRemove(req.params.id, (err, result) => {
            if(err) {
                res.json({msg: "Failed to Delete Event"});
            } else {
                res.json({msg: "Event Deleted Succsessfully"});
            }
        });
    });

    //REMOVE USER
    router.delete('/user/:id', (req, res, next) => {
        Users.findByIdAndRemove(req.params.id, (err, result) => {
            if(err) {
                res.json({msg: "Failed to Delete User"});
            } else {
                res.json({msg: "User Deleted Succsessfully"});
            }
        });
    });

    //REMOVE USER EVENT
    router.delete('/userEvent/:id', (req, res, next) => {
        UsersEvents.findByIdAndRemove(req.params.id, (err, result) => {
            if(err) {
                res.json({msg: "Failed to Delete UserEvent"});
            } else {
                res.json({msg: "UserEvent Deleted Succsessfully"});
            }
        });
    });

    //REMOVE GROUP
    router.delete('/group/:id', (req, res, next) => {
        Groups.findByIdAndRemove(req.params.id, (err, result) => {
            if(err) {
                res.json({msg: "Failed to Delete Group"});
            } else {
                res.json({msg: "Group Deleted Succsessfully"});
            }
        });
    });

    //REMOVE REQUEST
    router.delete('/request/:id', (req, res, next) => {
        Requests.findByIdAndRemove(req.params.id, (err, result) => {
            if(err) {
                res.json({msg: "Failed to Delete Request"});
            } else {
                res.json({msg: "Request Deleted Succsessfully"});
            }
        });
    });

    //REMOVE MEDIA
    router.delete('/media/:id', (req, res, next) => {
        Media.findByIdAndRemove(req.params.id, (err, result) => {
            if(err) {
                res.json({msg: "Failed to Delete Media"});
            } else {
                res.json({msg: "Media Deleted Succsessfully"});
            }
        });
    });

    //REMOVE USER INTEREST
    router.delete('/userInterest/:id', (req, res, next) => {
        UserInterests.findByIdAndRemove(req.params.id, (err, result) => {
            if(err) {
                res.json({msg: "Failed to Delete User Interest"});
            } else {
                res.json({msg: "User Interest Deleted Succsessfully"});
            }
        });
    });

    //REMOVE REVIEW
    router.delete('/review/:id', (req, res, next) => {
        Reviews.findByIdAndRemove(req.params.id, (err, result) => {
            if(err) {
                res.json({msg: "Failed to Delete User Interest"});
            } else {
                res.json({msg: "User Interest Deleted Succsessfully"});
            }
        });
    });

module.exports = router;